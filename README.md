### Description:
___
Simple REST API application that provides interaction intarface with file storageю
When a load is considered a file hash sum that determines the path to the file in the storage.

If hash is '7372ee9cca00d895' then path will be 'store/73/7372ee9cca00d895'

### Installation:
___
1.  Install *python 3.7+ using [documentation](https://docs.python.org/3/using/unix.html) for your distributions.
2. Create virtual environment using *virtualenv*:
   ```
    $ /path/to/python3 -m venv venv
   ```
3. Activate *virtualenv*:
   ```
    $ source /path/to/venv/bin/activate
   ```
4. Install requirements:
    ```
    $ pip install -r requirements.txt
    ```
5. Open file *.env* and set variables:
    ```
    BASE_PATH=/path/to/stotage/
    STORAGE_PATH=/path/to/stotage/store/
    SECRET_KEY=7372ee9cca00d895f21a6bf04eafc9bba020c3ec0de4f5b5
    HASH_LENGTH=16
    ```
6. Start Flask server:
    ```
    $ flask start
    ```
### Usage
___
+ Get current storage files tree:
  ```
  $ curl -X GET 127.0.0.1:5000/api/files
  ```
+ Upload /path/to/uploading/file to storage:
  ```
  $ curl -X POST -F file=@/path/to/uploading/file 127.0.0.1:5000/api/files
  ```
  Response will contain field *"hash"* -- use it in other requests.
+ Download file from storage by hash:
  ```
  $ curl -X GET 127.0.0.1:5000/api/files/<hash>
  ```
  Instead <hash> insert hash obtained when uploading
+ Delete file from storage by hash:
  ```
  $ curl -X DELETE 127.0.0.1:5000/api/files/<hash>
  ```
