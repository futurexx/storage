from werkzeug.exceptions import HTTPException
from flask import jsonify


_API_ERRORS = {
    'MissingFile': {
        'http_code': 400,
        'description': 'Request must contains field "file" \
        with name of uploading file.'
    },
    'InvalidHash': {
        'http_code': 400,
        'description': 'Request contains invalid hash.'
    },
    'FileNotFound': {
        'http_code': 404,
        'description': 'File with the specified hash is not in the storage'
    }
}


class MetaAPIException(type):
    """API exception metaclass"""

    def __getitem__(self, key):
        if key in _API_ERRORS:
            response = jsonify({
                'status': 'ERROR',
                'type': key,
                'description': _API_ERRORS[key]['description']
            })

            response.status_code = _API_ERRORS[key]['http_code']

            return type(key, (HTTPException,), {})(response=response)


class APIException(object, metaclass=MetaAPIException):
    """API exception container"""
    pass
