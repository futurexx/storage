from app import app, storage
from app.exceptions import appException
from flask import jsonify, request, redirect, url_for, send_file

# app short documentation
DOC = {
    'upload': {
        'url': '/api/files',
        'http_method': 'POST',
        'field': 'file',
        'description': 'Upload file on storage.'
    },
    'download': {
        'url': '/api/files/<hash>',
        'http_method': 'GET',
        'description': 'Download file by <hash>.'
    },
    'delete': {
        'url': '/api/files/<hash>',
        'http_method': 'DELETE',
        'description': 'Delete file by <hash>.'
    },
    'get_all': {
        'url': '/api/files',
        'http_method': 'GET',
        'description': 'Get current storage files tree.'
    }
}


@app.route('/', methods=['GET'])
def index():
    """url: '/'

    redirect 302 from '/' to '/api'
    """
    return redirect(url_for('main'), 302)


@app.route('/api', methods=['GET'])
def api():
    """url: '/api'

    return json with DOC
    """
    return jsonify(DOC)


@app.route('/api/files', methods=['GET', 'POST'])
def file_storage():
    """url: '/api/files'

    if POST request: uploading attached file
    if GET request: return storage files tree on json
    """
    if request.method == 'POST':
        if 'file' not in request.files:
            raise APIException['MissingFile']

        file = request.files['file']

        response = jsonify({
            'status': 'OK',
            'description': 'File successfully uploaded',
            'hash': storage.upload(file)
        })
        response.status_code = 201
    if request.method == 'GET':
        response = jsonify({'status': 'OK',
                            'description': 'Current storage files tree',
                            'storage_tree': storage.tree()})
        response.status_code = 204

    return response


@app.route('/api/files/<fhash>', methods=['GET', 'DELETE'])
def file_with_hash(fhash):
    """url: '/api/files/<hash>'

    if GET: using <hash> send file to client
    if DELETE: using <hash> delete file from storage
    """
    if len(fhash) < app.config['HASH_LENGTH']:
        raise APIException['InvalidHash']

    path_to_file = storage.search(fhash)

    if not path_to_file:
        raise APIException['FileNotFound']

    if request.method == 'GET':
        return send_file(path_to_file, as_attachment=True)

    if request.method == 'DELETE':
        storage.delete(path_to_file)

        response = jsonify({'status': 'OK',
                            'description': 'File successfully deleted'})
        response.status_code = 200

        return response
