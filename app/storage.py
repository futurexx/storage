import os
import hashlib

from app import app


def _hash_sum(file):
    """Calculate file hashsum by frames.

    Return sha256 hash
    """
    hasher = hashlib.sha256()

    # generator for reading file by frame
    def frames_iter(file, blocksize=4096):
        frame = file.read(blocksize)
        while len(frame) > 0:
            yield frame
            frame = file.read(blocksize)

    # get file hash sum
    for frame in frames_iter(file):
        hasher.update(frame)

    # return position of file to the begining
    file.seek(0)
    return hasher.hexdigest()[:app.config['HASH_LENGTH']]


def _prepare_path(file):
    """Create storage structure if not exist.

    Return abspath to uploading file.
    """
    filename = _hash_sum(file)
    root_path = os.path.join(app.config['STORAGE_PATH'], filename[:2])
    if not os.path.exists(root_path):
        os.makedirs(root_path)

    return os.path.join(root_path, filename)


def upload(file):
    """Prepare storage for uploding file and
    save file on storage.

    Return file hash.
    """
    path = _prepare_path(file)
    file.save(path)

    return os.path.basename(path)


def tree():
    """Getting current storage files tree.

    Return dict with dirs and files names.
    """
    tree = dict.fromkeys(os.listdir(app.config['STORAGE_PATH']))

    for subdir in tree:
        path = os.path.join(app.config['STORAGE_PATH'], subdir)
        tree[subdir] = os.listdir(path)

    return tree


def search(_hash):
    """Searching file in storage.

    Return /path/to/file if file exist or None.
    """
    path_to_file = os.path.join(app.config['STORAGE_PATH'], _hash[:2], _hash)
    if os.path.exists(path_to_file):
        return path_to_file


def delete(path):
    """Searching file on storage and delete it if exist.

    Return True if was found and deleted or False.
    """
    os.remove(path)

    try:
        os.rmdir(os.path.dirname(path))
    except OSError:
        pass
