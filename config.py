import os
from dotenv import load_dotenv


basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))


class Config(object):
    """App config class"""

    CSRF_ENABLED = True
    SECRET_KEY = os.environ.get('SECRET_KEY')
    BASE_PATH = os.environ.get('BASE_PATH')
    STORAGE_PATH = os.environ.get('STORAGE_PATH')
    HASH_LENGTH = int(os.environ.get('HASH_LENGTH'))
